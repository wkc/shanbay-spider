import util from './util';
import _ from 'lodash';

const log = util.createLogger({
    name: '#',
});

const baseURL = 'https://www.shanbay.com';

const getWordbook = async ()=>{
  const text = await util.get(baseURL+'/wordbook/6091/');
  util.dump('333', text);
  const $ = util.load(text);
  const r = $('.wordbook-wordlist-name a').map((i,v)=>$(v).attr('href')).get();
  if(r.length == 0){
    throw(new Error('getWordbookPage error'+text));
  }
  return r;
}

const getWordbookPage = async (url, page)=>{
  const text = await util.get(baseURL + url + '?page=' + page);
  const $ = util.load(text);
  const l = $('table td.span2').map((i,v)=>$(v).text()).get();
  const r = $('table td.span10').map((i,v)=>$(v).text()).get();
  const z = _.zip(l,r);
  const m = z.map(v=>({word:v[0], en:v[1]}));
  log.info({page:m, url, num:m.length});
}

const main = async ()=>{
  try{
    const pages = await getWordbook();
    log.info('pages', pages);
    pages.map( async url=>{
      log.info('url', url);
      try{
        for(let i=1; i<=20; i++){
          await getWordbookPage(url, i);
        }
      }catch(e){
        log.error(e);
      }
    });
  }catch(e){
    log.error(e);
  }
};

log.info('start');
try{
  main();
}catch(e){
  log.error(e);
}
