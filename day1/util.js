import bunyan from 'bunyan';
import request from 'co-request';
import _ from 'lodash';
import cheerio from 'cheerio';
import co from 'co';
import fetch from 'node-fetch';
import fs from 'fs';


const createLogger = ({name})=>{
  return bunyan.createLogger({
      name: name,
  });
};

const get = async (url)=>{
  const r = await fetch(url);
  const text = await r.text();
  return text;
}

const load = (html)=>cheerio.load(html);

const dump = (name, text)=>{
  fs.writeFileSync('log/'+name, text, 'utf8');
}

export default {createLogger, get, _, load, dump};

